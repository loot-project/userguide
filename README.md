# User guide

Visit at https://loot-project.gitlab.io/userguide
(build with Jekyll)

## How to contribute

### Post an issue

If you have spoted anything that should or could be fixed on this documentation, go to :

(https://gitlab.com/loot-project/userguide/issues/new)

and fill the form to let us know what need to be done.

### Get more involved

If you plan to get more involved in this documentation, you'll probably need to work on either the documentation files or the site theme.

The userguide is build on Jekyll and generated by Gitlab.
If you don't know anything about Jekyll or Gitlab, yous should first start by reading :
- [Jekyll documentation](https://jekyllrb.com/)
- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)

In Jekyll, content are written using Markdown. This is a really simple "markup" syntax to structure text content. Get there to know how to write a document in Markdown :
- https://daringfireball.net/projects/markdown/

Then you are ready !

### Install locally

A few steps will get you ready : 

- Follow the instructions at https://jekyllrb.com/docs/installation/ to get Jekyll up and running 

- Create a new folder and clone this repository  :
'''
git clone https://gitlab.com/loot-project/userguide.git
'''

- Run :
'''
bundler exec jekyll serve --config _config_local.yml
'''
to start jekyll local server, then you can access your local installation at http://localhost:4000

To push on the repository, either contact us or make a pull request.
