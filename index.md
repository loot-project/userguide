---
layout: default
title: Bienvenue
nav_order: 1
---

# Bienvenue !

Loot est une application web mobile pour suivre et partager les contributions et leurs rétributions au sein d'une communauté.
{: .fs-6 .fw-300 }

[Démarrer]({{site.baseurl}}/doc/getting-started/index.html){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 }
[A propos de Loot]({{site.baseurl}}/doc/about/index.html){: .btn .fs-5 .mb-4 .mb-md-0 }
[Cas d'usage]({{site.baseurl}}/doc/usecases/index.html){: .btn .fs-5 .mb-4 .mb-md-0 }

