---
layout: default
title: Coworking
parent: Cas d'usage
nav_order: 1
---

# Pour un Coworking

Dans un espace de Coworking, les usagers sont amenés a participer à la vie du lieu : accueil, petite réparation, cuisine...

Il est possible de mettre en place des systèmes contributifs à divers niveau de l'organisation de l'espace. Prenons par exemple la gestion de l'accueil.

## Ressouce

Le poste d'accueil est une ressource de l'espace. Situé à l'entrée du coworking, un bureau est dédié pour renseigner les usagers du lieu. Un coworker occupe la fonction d'accueil deux matinées par semaine. Avant sa première prise de fonction, il reçoit une formation d'une heure pour découvrir les modalités d'accueil et les outils utiles (documentation sur le lieu, enregistrement d'un nouvel usager, achat d'une prestation...)

## Contributeurs

Les contributeurs à cette ressource sont l'ensemble des coworkers à même de remplir cette fonction. Cela peut donc être une sous-partie de tous les coworkers. Dans ce cas, ce sera les coworkers usagers détenant un forfait régulier.

## Budget

On configurera deux budgets pour cette ressource : 

- un budget "Accueil" pour les rétributions des temps passés à l'accueil et celui dédié au temps passé à entretenir le bureau d'accueil (documentation, fourniture, signalétique...)

- un budget "Accueil - Formation" pour les temps passés à former les nouveaux coworkers souhaitant tenir l'accueil et le temps passé à créer les supports de formation. La rétribution sera proposé en "Points". Les points peuvent ensuite être converti selon une grille établie et révisée régulièrement en avantage dans le lieu (par exemple, 100 Points pour une heure de salle de réunion, 10 points pour une boisson...)

Il est possible de créer un seul budget "Accueil" dont le montant sera régulièrement renfloué au fil de son utilisation ou de créer des budgets mensuels (par exemple "Accueil - Janvier", "Accueil - Février", ...) pour isoler les crédits accordés de manière plus précise et régulière.

## Configuration

- Rédiger et rendez disponibles les règles des budgets (condition d'adhésion au budget, comportements souhaité, rétribution type, comment convertir ses points...)
- Créer les comptes de chaque contributeurs concernés
- Créer le budget "Accueil"
    + Nom : Accueil
    + Description : "Accueillir les nouveaux usagers et maintenir le bureau d'accueil en état"
    + Devise : Points, Symbole : pt
    + Budget alloué : 1000
    + Statut : Sur demande (Condition : avoir suivi la formation Accueil)
    + Suivi du temps passé : Oui
    + Tâches : "Accueil", "Achat fourniture", "Documentation"
- Créer le budget "Accueil-Formation"
    + Nom : Accueil - Formation
    + Description : "Former les coworkers pour l'accueil et améliorer le contenu de la formation"
    + Devise : Points, Symbole : pt
    + Budget alloué : 2000
    + Statut : Sur demande (Condition : être capable d'animer la formation Accueil)
    + Suivi du temps passé : Non
    + Tâches : "Formation", "Support", "Documentation"

## Exemple de règles des budgets

### Budget "Accueil"

Le Budget Accueil est dédié aux contributions autour de la ressource "Bureau d'accueil"

Le Bureau d'accueil est un poste fixe basé à l'entrée du coworking. Le coworker qui rempli la fonction d'accueil s'y installe pour renseigner les visiteurs et futurs coworkers. 
Les contributeurs de ce budget pariticipent aussi au maintient en état du bureau :
- Réimprimer des documents (grille de tarifs, feuille d'adhésion...)
- Réapprovisionner les petites fournitures
- Documenter la fonction sur le wiki du Coworking 

Il dispose de :
- Un ordinateur portable
- Une documentation générale sur le Coworking
- Une grille de tarifs des prestations
- Une feuille d'adhésion

Le contributeur doit avoir suivi la formation "Accueil" avant de pouvoir rejoindre le budget. [Contactez le référent](mailto:mail@mail.com) pour suivre la formation.

La devise du budget est le Point. Il peut être converti en avantages selon la grille et les modalités disponibles [ici](http://coworking.com/grille_point.html). Conservez bien votre numéro de rétribution pour obtenir vos avantages.


Le crédit accordé est régulièrement renfloué en fonction des moyens de la communauté. 

Il n'y a pas de règles en usage concernant le montant des rétributions.



