---
layout: default
title: Banque de temps
parent: Cas d'usage
nav_order: 4
---

# Pour une banque de temps

Une banque de temps permet à une communauté d'échanger des services et de comptabiliser leur participation suivant le temps passé.

## Ressouce

Dans ce système, Loot n'est pas utilisé pour un commun mais comme un journal des échanges entre les membres.

Si la banque d'échange utilise un système informatique ou manuel de gestion des échanges, cela pourrait aussi être une ressource et donc gérer en contributif avec Loot. Dans cet exemple, on ne prendra en compte que le journal des échanges.

## Contributeurs

Comme il n'y a pas réellement de ressource, il n'y a pas réellement de contributeurs, mais un ensemble d'usagers. Toute personne qui propose ou dispose d'un service est considérer comme usager.

## Budget

Il n'y a ici qu'un seul budget, celui des échanges. Il n'a pas de montant car il ne propose pas de rétributions


## Configuration

