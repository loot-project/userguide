---
layout: default
title: Evènement
parent: Cas d'usage
nav_order: 2
---

# Pour un évènement

Dans ce cas, on considérera un évènement qui serait construit comme un commun, c'est à dire une ressource globale dont la tenue de l'évènement lui-même représente un usage de la ressource. 

## Ressouce

Ce qui constitue la ressource est l'ensemble des documents, échanges documentés, format d'animation, ou tout autres productions que l'évènement aura permis de construire en amont et en aval et qui seront par la suite entretenus pour rester accessible et enrichis dans la durée. L'évènement lui-même est un usage particulier et éphémère de la ressource.

## Contributeurs

Les contributeurs regroupent l'ensemble des personnes qui auront produit un contenu ayant participé à la construction de l'évènement et sa documentation. Cela peut inclure les personnes participant à son organisation tout autant que des visiteurs s'engageant suite à l'évènement.

## Budget

Il y a de très nombreux budgets possibles pour cette ressource, en fonction de la taille de l'évènement et de la quantité de productions envisagées.
Dans cet exemple on prendre en compte un seul budget pour l'organisation qui aurait pu se découper en de nombreux budgets comme la logistique, la communication, etc...
On créera par ailleurs un autre budget pour la documentation des productions de l'évènement (captation et compte-rendu de conférence, productions participatives des visiteurs) ainsi qu'un budget dédié à la documentation des formats d'animation.
Plus particulièrement le budget de documentation pourra être ouvert aux visiteurs de l'évènement pour qu'ils puissent enregistrer leurs contributions même si ils n'interviennent que le jour "J". L'organisation permettra de leur créer des comptes sur place. Pour faciliter l'obtention des rétributions, on dédoublera ce budget, l'un sera en euros (pour ceux qui peuvent émettre des factures), l'autre sera en "Thumb up!", gratification symbolique qui permettra de mettre en avant son engagement dans le processus de documentation.

## Configuration

- Rédiger et rendez disponibles les règles des budgets (condition d'adhésion au budget, comportements souhaité, rétribution type, comment convertir ses points...)
- Créer les comptes de chaque contributeurs concernés
- Créer le budget "Organisation"
    + Nom : Organisation
    + Description : "Logistique & communication"
    + Devise : euros, Symbole : €
    + Budget alloué : 4000
    + Statut : Ouvert à tous
    + Suivi du temps passé : Oui
    + Tâches : "Logistique", "Communication", "Suivi budget", "Gestion des conférenciers"
- Créer le budget "Documentation (€)"
    + Nom : Documentation
    + Description : "Recueillir et mettre en forme les contenus"
    + Devise : euros, Symbole : €
    + Budget alloué : 2000
    + Statut : Ouvert à tous
    + Suivi du temps passé : Oui
    + Tâches : "Compte-rendu", "Captation", "Formalisation, Synthèse","Mise en forme"
- Créer le budget "Documentation (ThumbUp)"
    + Nom : Documentation
    + Description : "Recueillir et mettre en forme les contenus"
    + Devise : Thumb Up, Symbole : ThUp
    + Budget alloué : 10000
    + Statut : Ouvert à tous
    + Suivi du temps passé : Oui
    + Tâches : "Compte-rendu", "Captation", "Formalisation, Synthèse","Mise en forme"
- Créer le budget "Formats d'animation"
    + Nom : Formats d'animation
    + Description : "Documenter les formats d'animation"
    + Devise : euros, Symbole : €
    + Budget alloué : 3000
    + Statut : Sur demande (ouvert aux animateurs des formats)
    + Suivi du temps passé : Non
    + Tâches : "Design","Rédaction","Diffusion"

## Exemple de règles des budgets

### Budget "Documentation (€)"

Le Budget Documentation est dédié aux contributions autour de la production de contenu issus de l'évènement.

Il est ouvert à toute personne qui souhaite proposer :
- un compte-rendu de conférence ou d'échange
- une captation (audio ou vidéo) d'un échange
- la mise en forme d'un compte-rendu ou d'une captation (relecture, correction, montage, sous-titrage)
- la synthèse (texte ou multimédia) d'un ensemble de contenu

Les contributeurs devront mettre à disposition leur production en Creative Commons CC-BY-SA via le site internet de l'association.

Le budget est crédité d'un montant définitif de 2000€. Il n'y a pas de règles d'usage concernant le montant des rétributions. La devise du budget est l'euros. 

Pour obtenir votre rétribution vous devez envoyer une facture d'un montant TTC correspondant au montant de votre retribution en précisant bien le numéro de retribution correspondant.
Si vous ne disposez pas de moyen d'émettre des factures, rapprochez vous de l'association qui tentera de vous proposer des contreparties équitables ou veuillez utiliser le budget Documentation(Thumb up).

### Variante du budget "Documentation (Thumb up)"

Le budget est crédité d'un montant définitif de 10 000 ThumbUp. 
Le montant est libre, l'usage est de 1 ThumbUp par contribution, ou 2 si vous pensez avoir apporté une contribution significative à la documentaiton, 3 équivalent au prix Nobel du contributeur.

Vos contributions seront automatiquement rétribué à la fin de l'évènement et affiché sur le site internet de l'association.


