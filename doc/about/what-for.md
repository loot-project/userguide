---
layout: default
title: A quoi sert Loot ?
parent: A propos de Loot
nav_order: 2
---

# A quoi sert Loot ?

Maintenant que l'on sait ce que fait l'application, reste à savoir à quoi elle peut être utile !
Lorsqu'une communauté décide de mettre en place un système contributif (que cela soit ou non autour d'un bien commun), elle peut démarrer avec des outils simples, comme des feuilles de calcul d'un tableur par exemple, ou tout simplement, un carnet et un crayon.

Quelque soit la méthode utilisée, elle devra permettre au contributeur  :

- de recenser sa contribution
- de déterminer et recevoir sa rétribution en accord avec les règles fixées par la communauté et le budget contributif disponible

Si des outils simples permettent d'effectuer ces tâches simples à petite échelle, il devient très vite compliqué d'en garantir le suivi dès que le modèle s'étend à plus de tâches ou plus de contributeurs. Si le système proposé n'est pas adapté (trop lourd à gérer, trop compliqué, insuffisament mieux à jour...), il finira par nuire à la contribution globale du projet.

Les systèmes contributifs fonctionnement généralement autour de deux points-clés : la confiance et la transparence. Le contributeur est autonome, on lui fait donc confiance pour estimer sa juste rétribution dans le cadre proposé. La communauté est interdépendante, elle doit donc avoir accès à toutes les informations nécessaires pour adapter son action et prendre collectivement les bonnes décisions. Des outils inadaptés peuvent nuire à la transparence d'information des membres de la communauté ce qui peut conduire à détériorer la confiance. 

C'est donc à cela que Loot peut être utile : mettre en place un système simple et fiable de suivi de la contribution pour assurer transparence et confiance au sein d'une communauté.

Si vous pensez que Loot peut vous aider dans cette mission, poursuivez votre lecture. Nous verrons d'abord concrètement comment Loot fonctionne puis comment le mettre en oeuvre pour votre usage particulier. 

