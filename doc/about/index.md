---
layout: default
title: A propos de Loot
nav_order: 3
has_children: true
---

# A propos de Loot

**Loot est une application web mobile pour suivre et partager les contributions et leurs rétributions au sein d'une communauté.**