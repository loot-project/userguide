---
layout: default
title: Qu'est-ce que Loot ?
parent: A propos de Loot
nav_order: 1
---

# Qu'est-ce que Loot ?

## Loot est une application web mobile ...
Loot est une application qui est conçue pour être consulté via un navigateur internet, principalement celui de votre téléphone mobile. Elle fonctionnera aussi sur le navigateur de votre ordinateur, mais son ergonomie a été pensée pour un usage sur smartphone.

Loot doit donc être hébergé sur un serveur, comme un site internet. Cela peut être votre propre serveur, ou un serveur loué chez un fournisseur (comme OVH par exemple). Installer Loot va bien au delà de ce guide utilisateur. La documentation spécifique à l'installation est disponible sur [Gitlab](https://gitlab.com/loot-project/loot-app/blob/master/docs/install.md).

En tant qu'utilisateur, sachez juste que Loot utilise le langage PHP et le framework Symfony 5. Pour le reste, confiez cela à un administrateur système ou tout autre personne qui connait suffisament bien ces technologies pour mener à bien l'installation.

Une fois l'installation faite, votre administrateur vous fournira une adresse web, par exemple *https://loot.mondomaine.com* qui vous permettra d'accéder à Loot.

## ... pour suivre et partager les contributions...

Loot a été conçu pour répondre aux besoins de groupes de personnes qui travaillent autour de biens communs.

Les biens communs sont des ressources qui sont gérés collectivement par leurs usagers dans le but de la préserver et de la développer. Pour le moment, cette brève définition devrait suffire, mais si vous souhaitez en savoir plus sur cette notion de biens communs, vous pouvez commencer par cette adresse : http://encommuns.org

Les personnes qui oeuvrent à la préservation de ressources, d'un commun, le font en contribuant. Ce terme est important car il dit que ces personnes ne sont ni des salariés qui travailleraient sur le produit de leur entreprise, ni des bénévoles qui soutiennent l'action d'une association, mais des individus qui décident de mettre en oeuvre leur temps et leurs compétences pour assurer la pérénité de cette ressource. On les nomment "Contributeurs" car ils ne sont pas défini par un statut juridique ou social, mais par leur action auprès du commun, quelle qu'elle soit.

## ... et leurs rétributions ...

On l'a dit, œuvrer pour un commun ne veut pas dire être bénévole. Cela n'exclu donc pas que le contributeur puisse recevoir une contrepartie en échange de son implication. Toutefois, comme il n'est pas salarié ou prestataire d'une quelconque organisation, il est le seul juge de la nature et de la valeur de cette contrepartie. C'est ce que l'on appelle sa rétribution.

Ce qui est important à retenir, c'est que la rétribution :

- N'est pas un salaire. Il n'y a pas de subordination vis à vis d'un donneur d'ordre.
- Est individuelle. Une même action peut être rétribué différement pour des contributeurs différents.
- N'est pas systématiquement lié à la nature ou la durée de l'action menée.
- N'a pas de nature déterminée. Cela peut être de l'argent ou tout autre forme de reconnaissance.

Évidemment, cela peut sembler très ou trop libre, et donc ingérable. Mais le contributeur n'agit jamais seul. Il est lié à sa communauté, ce qui nous amène à la dernière partie de la définition.

## ... au sein d'une communauté

Communauté est à prendre au sens sociologiquement large, celui d'un groupe de personne qui interagissent et partagent un intérêt commun, en l'occurence, la préservation d'une ressource.

Très souvent, quand il s'agit de communs, la communauté est de fait l'ensemble des usagers de la ressource. Au sein de cette communauté, vous retrouverez les contributeurs que nous avons évoqués plus haut, ceux qui mène des actions concrètes envers le communs.

Cette communauté est légitime pour décider de ce qui est nécessaire à la préservation de sa ressource. Elle doit par ailleurs trouver les moyens de mettre en oeuvre ces actions. Cela peut impliquer du temps, des moyens matériels et financiers. Lorsqu'elle fait appel à des contributeurs, elle peut utiliser tout ou partie de ses moyens pour les rétribuer. C'est ce que l'on appellera par la suite le "budget contributif", élément au coeur de Loot.

Il donc aussi du ressort de la communauté de décider collectivement des règles qu'elle veut imposer à la rétribution, que cela soit pour des raisons pratiques, éthiques ou simplement par limitation de ses moyens.