---
layout: default
title: Comment utiliser Loot ?
parent: A propos de Loot
nav_order: 3
---


# Comment utiliser Loot ?

Avant de commencer à utiliser l'outil, nous vous conseillons de déterminer un certain nombre de règles d'usage qui correspondent à vos ressources et à votre communauté. Loot est un outil très simple mais il ne sera efficace que si vous savez l'utilisez conformément à vos objectifs.

Pour cela, voici quelques éléments clés qui vous permettront d'en tirer le meilleur parti, ainsi que quelques cas d'usages dans lesquels vous vous reconnaitrez peut-être ou qui vous serviront de point de départ.

## Préparatifs

Pour accompagner les préparatifs, nous vous proposons un exemple continu en illustration. Il s'agit d'une communauté d'usager participant à la vie d'un tiers-lieu proposant un espace de coworking, une grande salle de réunion et un fablab.

### Les contributeurs

Votre communauté, c'est l'ensemble des usagers de vos ressources. Parmi eux, certains sont contributeurs, c'est à dire qu'ils mènent des actions concrètes dans votre projet. C'est principalement à eux que s'adresse Loot, même si les usagers non contributeurs pourraient être intéressé pour consulter ce qui est saisi dans l'application.

Vous devez donc d'abord vous demander : 

> Qui sont les contributeurs de ma communauté ? 

Cela peut paraître trivial, mais êtes-vous bien capable de les identifier ? Est-ce que cette notion est bien claire et partagée au sein de votre communauté ?

Si nécessaire, vous pouvez planifier une session collective qui permettra à tous de bien identifier et comprendre la posture du contributeur. Ces contributeurs seront les futurs usagers de Loot, il faut donc aussi qu'ils comprennent bien l'intérêt, tant pour eux que pour la communauté, d'utiliser correctement l'outil en mettant en avant les notions de confiance et transparence du modèle contributif.

Vous pourriez aussi avoir besoin de définir des règles précises qui conditionnent le "statut" de contributeur. Est-il librement déterminé par les membres de la communauté ? Ne concerne-t-il que certaines actions particulières ? Faut-il une ancienneté minimum ? Avoir suivi une formation ?
Si vous devez imposer de restrictions au statut de contributeur, mieux vaut le faire avant de passer sur Loot. Il sera plus difficile et désagréable de supprimer autoritairement des comptes utilisateur par la suite.

#### Exemple
Dans un tiers-lieu, il y a de nombreux usagers : coworkers, usagers de passage, locataires de la salle de réunion, membres du fablab... Au sein de ces usagers, quels sont ceux qui participent à la vie du lieu ? Tout le monde ? Dans ce cas, tous les usagers sont contributeurs. Mais vous pourriez décider de ne pas inclure les usagers de passage car il ne contribue pas "activement" à la vie quotidienne du lieu ou ne sont pas assez disponible pour participer à sa gouvernance. Idem pour les locataires non-coworker de la salle de réunion. Il ne sont pas des usagers mais des consommateurs-clients de la ressource.


### Les ressources

Au delà de ses fonctionnalités, Loot est pensé pour vous aider à préserver collectivement des ressources. Pour cela, il faut bien avoir identifié les ressources pour lesquelless les contributeurs se mobilisent.

Cela peut parfois être difficile de bien définir le périmètre d'une ressource. Dans le cas d'un lieu par exemple, doit-on prendre en compte la totalité du lieu défini par son périmètre géographique, certains espaces seulement, certains usages... Les cas sont trop nombreux pour donner des règles simples, mais un bon point de départ est :

> Est-ce que je peux définir ma (mes) ressource(s) en une phrase ?

Si à travers cette phrase, il n'y a pas d'ambiguité pour les membres de la communauté (et même au-delà) sur ce qui est désigné comme cette ressource, c'est déjà un grand pas vers une gestion efficace de celle-ci. Si cela s'avère compliqué, c'est qu'il s'agit peut-être d'isoler plusieurs ressources distinctes qui seront elle plus facile à gérer. 

#### Exemple 
Votre tiers-lieu dispose d'un espace coworking, d'une salle de réunion et d'un fablab. Est-ce une seule et même ressource. Les usagers de l'un sont-ils usagers de l'autre ? Sont-ils tous concernés de la même façon ? Peut-être faut-il envisager de isoler trois ressources différents avec trois communautés, quitte à ce que certains usagers soient membres de plusieurs communautés en même temps. A l'avenir, cela facilitera la gestion des contributions en isolant les problématiques de chacun. Vous pouvez tout autant décider de ne pas gérer la salle de réunion avec un système contributif dans un premier temps.

### Les budgets

Le budget (contributif) regroupe les moyens mis à disposition par la communauté pour assurer la rétribution de ses contributeurs. Cela peut représenter tout ou partie de l'ensemble de ses moyens. Pour cela, il faut d'abord évaluer ce qui, parmi les moyens globaux de la communauté, est d'ors et déjà affecté à des budgets non contributifs, comme des frais, des factures de prestations par exemple.

Il faut aussi se poser la question de l'importance pour les contributeurs de leur rétribution. Est-ce crucial pour eux ? Est-ce juste une forme de reconnaissance ? Les contributeurs sont-ils tous dans le même cas ou existe-t-il de grande disparité dans leurs conditions. Ce sont des questions difficiles. Et bien sur, les réponses peuvent évoluer au fil du temps. Commencez par vous demandez : 

> Quels sont les moyens minimaux dont les contributeurs ont besoin pour continuer à oeuvrer pour la ressource ? 

Votre budget doit d'abord répondre à ce premier niveau. Si il ne le permet pas, vos contributeurs pourraient se désinvestir. Cela peut-être des moyens financiers, mais certains contributeurs dont les moyens sont assurés par ailleurs peuvent espérer une rétribution sous la forme d'une reconnaissance, d'un service particulier...

Dans Loot, cela revêt une importance particulière car chaque budget correspondra à une "enveloppe" dans laquelle les contributeurs d'une ressource pourront aller prélever une partie. Aussi à une ressource pourra correspondre plusieurs budgets de plusieurs types différents (euros, points, coupon, monnaie locale,...) avec des options différentes (limité, illimité,...).

Dans tous les cas, prenez le temps de prendre en compte ces questions avant de créer un budget. C'est ce qui rendra efficace et cohérent votre système contributif.

#### Exemple 
Au sein du tiers-lieu, le coworking a été identifié comme une ressource à part, avec sa propre communauté. Il y a 3 budgets associés à cette ressource. Le premièr est en euros, et il est destiné aux contributeurs assurant les fonctions essentielles de l'espace. Le deuxième, est un système de point. Les contributeurs se rétribuent en point lorsqu'il effectue une petite tâche quotidienne et peuvent ensuite transformer ces points en journée de coworking ou produits de la cafétaria. Le troisième est illimité et sert uniquement de "journal des contributions".

### La rétribution

Lorsque vos utilisateurs auront réalisé quelques contributions, il pourront alors demander à être rétribué pour cela, selon les règles en usages dans votre communauté.

Il n'y a pas de modalité universelle pour cela. C'est pour cela que Loot se limite à la déclaration des contributions et des rétributions. Le versement de ces rétributions appartient à votre organisation. Chaque contribution (ou groupe de contributions) sera enregistré selon un numéro unique de rétribution qui identifiera les contributions liées et leur montant. Vous devez dans vos règles définir le process de versement de cette rétribution. 

> Comment s'assurer que les rétributions seront versées ?

#### Exemple 
Dans l'espace de Coworking, les contributeurs à l'Accueil, se versent une rétribution en Points pour chacune de leur contribution. Après enregistrement, il obtient un numéro unique qui totalise les Points de ses contributions. Il peut alors se rendre au Fablab et utiliser ses points pour utiliser du temps machine de la découpe Laser.