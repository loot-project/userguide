---
layout: default
title: Démarrer
nav_order: 2
has_children: true
---

# Démarrer avec Loot

A ce stade, vous pouvez utiliser la [version de démonstration](http://loot-demo.soletic.org/login){:target="_blank" rel="noopener"} en ligne de Loot. Elle vous permettra de faire vos premiers pas, mais aucune de vos données ne sera conservées. Si vous voulez vous lancer directement dans votre projet, c'est le moment de [lancer l'installation](https://gitlab.com/loot-project/loot-app/blob/master/docs/install.md).

[Introduction - Glossaire]({{site.baseurl}}/doc/getting-started/intro-glossaire.html)

[Se connecter pour la première fois]({{site.baseurl}}/doc/getting-started/first-login.html)

[Créer le premier utilisateur]({{site.baseurl}}/doc/getting-started/first-user.html)

[Créer un budget]({{site.baseurl}}/doc/getting-started/create-budget.html)

[Déclarer une contribution]({{site.baseurl}}/doc/getting-started/log-contribution.html)

[Déclarer une rétribution]({{site.baseurl}}/doc/getting-started/claim-contribution.html)


