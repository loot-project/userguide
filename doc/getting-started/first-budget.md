---
layout: default
title: Créer un budget
parent : Démarrer
---

# Créer un premier budget

Maintenant que vous êtes connecté en tant qu'utilisateur (et non en tant que root), vous pouvez créer votre premier budget.

Dans la navigation principale, cliquez sur Budget > Ajouter un budget

Remplissez le formulaire et cliquez sur Enregistrer.

Le formulaire est simple. Mais vous devez bien prendre le temps de réfléchir à la configuration de votre budget car il est le reflet du fonctionnement de votre communauté autour de cette ressource. N'hésitez pas à lire au préalable [Comment utiliser Loot]({{site.baseurl}}/doc/about/how-to.html) et à consulter les [Cas d'usages]({{site.baseurl}}/doc/usecases/index.html)