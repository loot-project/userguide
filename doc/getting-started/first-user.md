---
layout: default
title: Créer un utilisateur
parent : Démarrer
---

# Créer un premier utilisateur

Vous devez à présent être connecter en tant que "root". En dehors de cas particulier, c'est la seule fois où cela sera nécessaire. 

![Main navigation]({{site.baseurl}}/assets/images/screen/navigation.png)

Pour créer votre premier compte, ouvrez la navigation principale de l'application et cliquez sur le menu "Contributeurs > Ajouter un contributeur".

![Main navigation]({{site.baseurl}}/assets/images/screen/contributors-index.png)

Remplissez le formulaire et cliquer sur "Ajouter l'utilisateur".

![Main navigation]({{site.baseurl}}/assets/images/screen/contributors-create.png)

Vous êtes redirigé vers la liste des utilisateurs de l'application où votre premier compte apparait maintenant. L'application vous a généré un mot de passe provisoire. Notez le bien ! Vous pourrez par la suite le modifier. 

Ne commencez pas à utiliser l'application dès maintenant. Vous devez d'abord vous déconnecter de l'utilisateur "root". Pour cela rendez-vous dans la navigation principale et cliquez sur "Se déconnecter" sous le nom d'utilisateur "root". 

Vous êtes revenu à la page d'identification et vous pouvez maintenant vous connecter avec les identifiants du nouvel utilisateur que vous venez de créer.
