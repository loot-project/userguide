---
layout: default
title: Introduction et glossaire
parent : Démarrer
nav_order: 1
---

# Introduction

## Le budget contributif

Le budget contributif est une somme financière dédiée à financer les interventions de membres contributeurs-trices à la condition qu’elles participent à l’objectif du budget. Il est animé par une personne avec l’intention de rendre le plus libre possible le choix pour chacun de choisir ce qui est le plus nécessaire de faire dans le cadre de ce budget. Il peut donc être utilisé dans de nombreux contextes et cette page de Movilab permet d’explorer la corémuneration dans un budget contributif.

> Loot est un outil pour faciliter la saisie des contributions à un budget contributif tout en assurant une transparence des rétributions effectives. [Un tableau de bord](https://gitlab.com/loot-project/loot-dashboard) permet de rendre visible la dynamique de contributions et d'effectuer des analyses.

## Le cobudget

Le cobudget est un outil et une méthodologie qui permet à tous les membres d’une organisation de s’impliquer dans la prise de décision en proposant des projets et en allouant des fonds aux propositions qui leur plaisent.

> Loot n'est pas un outil pour animer ou piloter le processus de cobudget.

Ce schéma permet d'appréhender globalement la dynamique collective induite par ces outils d’organisation financière (budget contributif et processus de cobudget) :

![Login Screen]({{site.baseurl}}/assets/images/Cobudget-et-budgets-contributifs-1024x643.jpg)

## Pourquoi une organisation adopterait les outils de budgets contributifs ?

Ce sont des outils et des méthodes de gestion financière participative pour impliquer chaque membre d’une communauté dans les différentes activités qu’elle déploie !

- Pour gouverner collectivement les flux financiers
- Pour mixer bénévolat et rétribution financière sans tensions
- Pour reconnaitre la valeur produite
- Pour donner de la liberté d’action aux membres
- Pour utiliser efficacement, juste ce qu’il faut et au bon moment, les ressources financières

[Différents cas d'usage sont proposés ici]({{site.baseurl}}/userguide/doc/usecases/)

## Ressources complémentaires

- [La corémuneration dans un budget contributif](https://movilab.org/wiki/Coremuneration) sur Movilab
