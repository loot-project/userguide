---
layout: default
title: Première connexion
parent : Démarrer
---

# Se connecter pour la première fois

A l'installation, votre application dispose d'un compte utilisateur unique et qui dispose de tous les droits. C'est l'utilisateur "root". Cet utilisateur ne doit pas être utilisé pour un usage courant de l'application. 
Pour utiliser l'application vous devez créer de nouveaux comptes pour les usagers. Et pour cela, vous devez d'abord vous identifier en tant que "root". Si votre administrateur n'a pas modifier la configuration par défaut de Loot, le mot de passe de "root" est "root". Il est préférable de modifier ce mot de passe à l'installation pour des raisons de sécurité. Votre administrateur vous le communiquera lors que l'installation sera finie.

![Login Screen]({{site.baseurl}}/assets/images/screen/login.png)

>Sur la page d'accueil, identifiez vous avec le compte root par défaut (identifiant "root", mot de passe "root"), ou celui donné par votre administrateur.