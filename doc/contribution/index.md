---
layout: default
title: Contribuer à Loot
nav_order: 5
has_children: true
---

# Contribuer à Loot

**Loot est une application open source. Vous pouvez accéder à son code source, le réutiliser, le modifier selon les termes de la licence [AGPLv3](https://gitlab.com/loot-project/loot-app/-/blob/master/LICENSE).**

Bien sûr, ouvrir le code d'une application, c'est offrir de la transparence à ses usagers, mais c'est aussi offrir la possibilité de participer à son évolution. Quelque soit vos compétences, vous pouvez toujours aider à faire de Loot un meilleur outil. Suivez le guide ci-dessous selon le profil dont vous vous sentez le plus proche.

**Merci par avance de votre participation !**