---
layout: default
title: En tant que développeur
parent : Contribuer à Loot
---

Loot est ouvert à toutes les opportunités qui pourrait le rendre meilleur :-) 

# Workflow de duplication (contributeur externe)
Si vous souhaitez collaborer ponctuellement au développement, résoudre un ticket, proposer un plugin... nous suivons un workflow de duplication.

Vous en saurez plus en lisant ceci : 
[Workflow de duplication](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/forking-workflow)


# Workflow du dépôt principal (équipe de mainteneurs)
Le développement et la maintenance principale de Loot est assurée par une petite équipe de développeur qui en assure la stabilité et la "philosophie" générale. 

Techniquement, nous suivons ce workflow : 
[Workflow Gitflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow)

Si vous souhaitez vous investir plus dans le développement de Loot, contactez un des mainteneurs principaux :
- [Laurent Chedanne](https://blog.chedanne.pro/)
- [Pierre Trendel](https://anis-catalyst.org/)
- Daishi Kaszer

# Contributions et rétribution
Evidemment les développeurs de Loot utilise Loot :-)

Le projet est aujourd'hui soutenu financièrement pour permettre aux développeurs-mainteneurs de se rétribuer quand il le souhaite.
*(Ajouter les logos...)*

Si vous souhaitez soutenir le projet, participez à son financement, contacter l'[ANIS](https://anis-catalyst.org/)
