---
layout: default
title: En tant qu'usager
parent : Contribuer à Loot
---

Le développement de Loot est basé sur les besoins de utilisateurs. Son objectifs reste, et doit rester, simple : faciliter le suivi des budgets contributifs. 

Mais garder cet objectif simple ne veut pas dire ne plus y toucher. Bien au contraire, nous sommes à l'écoute de toutes les suggestions de la part des usagers qui pourrait rendre toujours plus simple et agréable son utilisation.  

Les choix de développement de Loot sont décidés par une petite équipe de développeurs, les mainteneurs, qui s'investissent régulièrement dans son évolution. A partir de vos propositions, des remontées des différentes communautés, ce petit groupe tente de concilier le temps disponible, le maintien de la ligne directrice du projet et son ergonomie, et cela si possible, en toute transparence.

Vous pouvez avoir un aperçu de ce processus en jetant un oeil sur le [tableau projet](https://trello.com/b/vE0RhXAE/loot) et les [tickets](https://gitlab.com/loot-project/loot-app/-/issues) du gestionnaire de code source. 

# Proposer

Pour proposer une amélioration, signaler un bug, etc... direction le [gestionnaire de tickets](https://gitlab.com/loot-project/loot-app/-/issues). Sur cet écran, utilisez le bouton "Nouveau ticket" en haut à droite et saisissez votre proposition. Quelques conseils : 

- **Rédiger un titre court et explicite** : il doit refléter clairement l'intention de votre proposition sans rentrer dans les détails. Comme cela, en parcourant la liste des tickets, les développeurs et les autres usagers prennent connaissance facilement de ce qui est déjà en cours ou proposé.
- **Décrivez précisement votre description** : vous pouvez jeter un oeil aux autres tickets pour voir comment ils ont été formulé. Si ce n'est pas une obligation, essayez de respecter le format en deux paragraphe : l'un présente la situation que vous rencontrez, l'autre la proposition d'amélioration ou de correction. N'ayez pas peur d'être précis, ce qui vous semble évident ne l'est pas forcément pour tous !

Les mainteneurs auront alors connaissance de votre proposition. A partir de là, on pourra en discuter via les tickets, et l'intégrer si nécessaire dans le planning de développement de l'application.

# Documenter

La documentation est essentielle à l'usage. Si l'ergonomie de l'application tente d'être le plus explicite possible (et donc de se passer de documentation), il est parfois bien utile de pouvoir trouver de l'aide. Vous êtes actuellement en train de lire la documentation du projet et si vous l'avez déjà trouvé utile, c'est parfait ! Mais si il vous prend l'envie de l'agrémenter, de l'améliorer, de proposer des tutos, vidéos, ou n'importe quel autre format qui en rendrait l'usage plus agréable, vous êtes les bienvenus !

Comme pour l'application, la documentation est ouverte sur un gestionnaire de code (le site que vous consultez actuellement est générer automatiquement à partir de simples fichiers textes) à cette [adresse](https://gitlab.com/loot-project/userguide).

[Le gestionnaire de ticket](https://gitlab.com/loot-project/userguide/-/issues) vous est grand ouvert ! Pour particiepr plus activement à la documentation, contacter un mainteneur via l'[association ANIS](https://anis-catalyst.org/). 

# Tester, faire des retours

Si vous ou un proche avez des compétences techniques, jetez un oeil du côté de [notre processus de développement](({{site.baseurl}}/doc/contribution/as-a-developper.html)) et au [gestionnaire de code](https://gitlab.com/loot-project/docker-compose-loot) dédiée à l'installation de Loot. Vous pouvez alors installer votre propre copie de Loot et modifier le code, l'ergonomie, etc... Tenez-nous au courant, on aura alors très envie de voir ce que vous faites et pourquoi pas intégrer vos adaptations à l'application coeur.

